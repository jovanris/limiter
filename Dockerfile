FROM golang:latest

WORKDIR /app

ENV SRC_DIR=/go/src/github.com/jovanris/limiter/
ENV GO111MODULE=on
# Add the source code:
ADD . $SRC_DIR
# Build it:
RUN cd $SRC_DIR; go mod vendor; go build -o myapp; cp myapp /app/; cp config.toml /app/

ENTRYPOINT ["./myapp"]