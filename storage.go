package main

import (
	"errors"
	"sync"
	"time"
)

// Transaction store transaction data
type Transaction struct {
	ID           int
	Count        int
	CreationTime int64
}

var storage map[int]Transaction
var availableStorage []int
var mutex sync.Mutex
var signal chan struct{}

func initStorage() {
	// Close already existing channel
	if signal != nil {
		close(signal)
	}

	// Make new channel
	signal := make(chan struct{})
	_ = signal

	mutex.Lock()
	storage = make(map[int]Transaction)
	availableStorage = make([]int, config.MaxTransactions)
	for i := 0; i < config.MaxTransactions; i++ {
		availableStorage[i] = i
	}
	mutex.Unlock()
	go cleanStorage()
}

// create Store a new transaction with count set to 1
func create() (Transaction, error) {
	mutex.Lock()
	if len(availableStorage) == 0 {
		mutex.Unlock()
		return Transaction{ID: -1, Count: 1}, errors.New("No available transaction ID")
	}

	id := availableStorage[0]
	availableStorage = availableStorage[1:]

	t := Transaction{ID: id, Count: 0, CreationTime: time.Now().Unix()}

	storage[id] = t
	mutex.Unlock()
	return t, nil
}

// get Return the transation by id
func get(transactionID int) (Transaction, error) {
	if validateTransaction(transactionID) == false {
		return Transaction{ID: -1, Count: 1}, errors.New("Transaction expired")
	}
	mutex.Lock()
	t, ok := storage[transactionID]
	mutex.Unlock()
	if ok {
		return t, nil
	}
	return Transaction{ID: -1, Count: 1}, errors.New("Transaction ID does not exist")
}

// update Increase the count of a transaction by 1
func update(t Transaction) (Transaction, error) {
	if validateTransaction(t.ID) == false {
		return t, errors.New("Transaction expired")
	}
	if t.Count < config.Limit {
		t.Count++
		mutex.Lock()
		storage[t.ID] = t
		mutex.Unlock()
		return t, nil
	}
	return t, errors.New("Transaction exceeds limit")
}

func remove(transactionID int) {
	mutex.Lock()
	_, ok := storage[transactionID]
	if ok {
		availableStorage = append(availableStorage, transactionID)
		delete(storage, transactionID)
	}
	mutex.Unlock()
}

// validateTransaction Removes expired transaction
func validateTransaction(transactionID int) bool {
	mutex.Lock()
	t, ok := storage[transactionID]
	mutex.Unlock()
	if ok {
		if t.CreationTime+int64(config.Lifetime) < time.Now().Unix() {
			return false
		}
	}
	return true
}

// cleanStorage Remove expired transactions fro mstorage periodically
func cleanStorage() {
	ticker := time.NewTicker(time.Duration(config.CleanInterval) * time.Second)
	for {
		select {
		case <-signal:
			ticker.Stop()
			return
		case <-ticker.C:
			mutex.Lock()
			for s := range storage {
				if storage[s].CreationTime+int64(config.Lifetime) < time.Now().Unix() {
					delete(storage, s)
					availableStorage = append(availableStorage, s)
				}
			}
			mutex.Unlock()
		}
	}

}
