// +build integration all

package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

func TestHandleGet(t *testing.T) {
	// Test non existant transaction
	req, err := http.NewRequest("GET", "/0", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	Router().ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	expected := "Transaction ID does not exist\n"
	actual := rr.Body.String()
	if actual != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual, expected)
	}

	// Test existing transaction
	create()
	rr = httptest.NewRecorder()

	Router().ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected = `{"ID":0,"Count":0}`
	actual = rr.Body.String()
	if actual != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual, expected)
	}

	// Test invalid input
	req, err = http.NewRequest("GET", "/fail", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()

	Router().ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

}

func TestHandlePost(t *testing.T) {
	initStorage()
	config.Limit = 1
	// Test non existant transaction
	req, err := http.NewRequest("POST", "/0", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	Router().ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNotFound {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusNotFound)
	}

	expected := "Transaction ID does not exist\n"
	actual := rr.Body.String()
	if actual != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual, expected)
	}

	// Test existing transaction
	create()
	rr = httptest.NewRecorder()

	Router().ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected = `{"ID":0,"Count":1}`
	actual = rr.Body.String()
	if actual != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual, expected)
	}

	// Test existing transaction exceeds limit
	create()
	rr = httptest.NewRecorder()

	Router().ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected = "Transaction exceeds limit\n"
	actual = rr.Body.String()
	if actual != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual, expected)
	}

	// Test invalid input
	req, err = http.NewRequest("POST", "/fail", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()

	Router().ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

func TestHandlePut(t *testing.T) {
	config.MaxTransactions = 1
	config.CleanInterval = 3
	config.Lifetime = 2
	initStorage()

	// Test create transaction
	req, err := http.NewRequest("PUT", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	Router().ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := `{"ID":0,"Count":0}`
	actual := rr.Body.String()
	if actual != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual, expected)
	}

	// Test unable to create transaction
	rr = httptest.NewRecorder()

	Router().ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNotAcceptable {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected = "No available transaction ID\n"
	actual = rr.Body.String()
	if actual != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual, expected)
	}

	// Test storage cleaning

	// Wait for transaction to expire
	time.Sleep(3 * time.Second)

	rr = httptest.NewRecorder()

	Router().ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusCreated {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected = `{"ID":0,"Count":0}`
	actual = rr.Body.String()
	if actual != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual, expected)
	}
}

func TestHandleDelete(t *testing.T) {
	// Test delete transaction
	req, err := http.NewRequest("DELETE", "/0", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	Router().ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusAccepted {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	expected := ``
	actual := rr.Body.String()
	if actual != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			actual, expected)
	}

	// Test invalid input
	req, err = http.NewRequest("DELETE", "/fail", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr = httptest.NewRecorder()

	Router().ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusInternalServerError {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

}
