package main

import (
	"fmt"

	"github.com/BurntSushi/toml"
)

// Configuration holds config valeus for the app
type Configuration struct {
	Lifetime        int `toml:"lifetime"`
	Limit           int `toml:"limit"`
	MaxTransactions int `toml:"max_transactions"`
	CleanInterval   int `toml:"clean_interval"`
}

var config Configuration

func loadConfig() {
	if _, err := toml.DecodeFile("./config.toml", &config); err != nil {
		fmt.Println(err.Error())
	}
}
