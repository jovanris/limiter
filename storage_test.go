// +build unit all

package main

import (
	"testing"
	"time"
)

func TestInitStorage(t *testing.T) {
	if storage == nil {
		t.Errorf("Storage should be initialized")
	}
}

func TestCreate(t *testing.T) {
	config.MaxTransactions = 1
	initStorage()

	tr, err := create()

	expected := Transaction{ID: 0, Count: 0, CreationTime: time.Now().Unix()}
	if tr != expected {
		t.Errorf("Object mismatch: %d-%d-%d *** %d-%d-%d",
			tr.ID,
			tr.Count,
			tr.CreationTime,
			expected.ID,
			expected.Count,
			expected.CreationTime,
		)
	}

	_, err = create()
	if err == nil {
		t.Errorf("Creation should fail")
	}

}

func TestGet(t *testing.T) {
	create()

	tr, err := get(0)
	expected := Transaction{ID: 0, Count: 0, CreationTime: time.Now().Unix()}

	if tr != expected {
		t.Errorf("Object mismatch: %d-%d-%d *** %d-%d-%d",
			tr.ID,
			tr.Count,
			tr.CreationTime,
			expected.ID,
			expected.Count,
			expected.CreationTime,
		)
	}

	tr, err = get(1)

	if err.Error() != "Transaction ID does not exist" {
		t.Errorf("%s", err.Error())
	}

	tr.CreationTime = tr.CreationTime - 1000
	storage[0] = tr
	_, err = get(0)
	if err.Error() != "Transaction expired" {
		t.Errorf("Transaction should be expired")
	}
}

func TestUpdate(t *testing.T) {
	config.Limit = 1
	initStorage()
	tr, _ := create()
	expected := Transaction{ID: 0, Count: 0, CreationTime: time.Now().Unix()}

	if tr != expected {
		t.Errorf("Object mismatch: %d-%d-%d *** %d-%d-%d",
			tr.ID,
			tr.Count,
			tr.CreationTime,
			expected.ID,
			expected.Count,
			expected.CreationTime,
		)
	}

	tr, _ = update(tr)
	expected = Transaction{ID: 0, Count: 1, CreationTime: time.Now().Unix()}
	if tr != expected {
		t.Errorf("Object mismatch: %d-%d-%d *** %d-%d-%d",
			tr.ID,
			tr.Count,
			tr.CreationTime,
			expected.ID,
			expected.Count,
			expected.CreationTime,
		)
	}

	tr, err := update(tr)
	if err.Error() != "Transaction exceeds limit" {
		t.Errorf("Transaction should exceed limit")
	}

	tr.CreationTime = tr.CreationTime - 1000
	storage[0] = tr
	tr, err = update(tr)
	if err.Error() != "Transaction expired" {
		t.Errorf("Transaction should be expired")
	}
}

func TestRemove(t *testing.T) {
	initStorage()
	if len(availableStorage) != config.MaxTransactions {
		t.Errorf("Avaliable transaction storage should be %d", config.MaxTransactions)
	}

	create()

	if len(availableStorage) != config.MaxTransactions-1 {
		t.Errorf("Avaliable transaction storage should be %d", config.MaxTransactions-1)
	}

	remove(0)

	if len(availableStorage) != config.MaxTransactions {
		t.Errorf("Avaliable transaction storage should be %d", config.MaxTransactions)
	}
	if availableStorage[config.MaxTransactions-1] != 0 {
		t.Errorf("Last element should be 0")
	}
}
