package main

import (
	"os"
	"testing"
)

func TestMain(m *testing.M) {
	loadConfig()
	initStorage()
	code := m.Run()
	os.Exit(code)
}
