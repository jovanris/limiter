package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func main() {
	loadConfig()
	initStorage()

	http.Handle("/", Router())
	fmt.Println("Starting up on 8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}

// Router initiates routes
func Router() *mux.Router {
	r := mux.NewRouter()

	r.HandleFunc("/", handlePut).Methods("PUT")
	r.HandleFunc("/{id}", handleGet).Methods("GET")
	r.HandleFunc("/{id}", handleDelete).Methods("DELETE")
	r.HandleFunc("/{id}", handlePost).Methods("POST")

	return r
}
